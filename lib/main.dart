import 'package:flutter/material.dart';
import 'package:flutter_calc/CalcButton.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see
        // the application has a blue toolbar. Then, without quitting
        // the app, try changing the primarySwatch below to Colors.green
        // and press "r" in the console where you ran "flutter run".
        // We call this a "hot reload". Notice that the counter didn't
        // reset back to zero -- the application is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Flutter Calculator'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful,
  // meaning that it has a State object (defined below) that contains
  // fields that affect how it looks.

  // This class is the configuration for the state. It holds the
  // values (in this case the title) provided by the parent (in this
  // case the App widget) and used by the build method of the State.
  // Fields in a Widget subclass are always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that
      // something has changed in this State, which causes it to rerun
      // the build method below so that the display can reflect the
      // updated values. If we changed _counter without calling
      // setState(), then the build method would not be called again,
      // and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance
    // as done by the _incrementCounter method above.
    // The Flutter framework has been optimized to make rerunning
    // build methods fast, so that you can just rebuild anything that
    // needs updating rather than having to individually change
    // instances of widgets.
    return new Scaffold(
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that
        // was created by the App.build method, and use it to set
        // our appbar title.
        title: new Text(config.title),
      ),
      body: new Center(
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new FormField<InputValue>(
                          initialValue: InputValue.empty,
                          builder: (FormFieldState<InputValue> field) {
                            return new Input(
                                key: new Key("numberDisplay"),
                                hintText: 'e.g. 3+4',
                                labelText: 'Name',
                                value: field.value,
                                onChanged: field.onChanged,
                                );
                          })
                    ]
                ),
                new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new CalcButton(
                          child: new Text("1"),
                          numberDisplay: new Input()
                      ),
                      new RaisedButton(
                          child: new Text("2")
                      ),
                      new RaisedButton(
                          child: new Text("3")
                      ),
                    ]
                ),
                new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new RaisedButton(
                        child: new Text("4"),
                      ),
                      new RaisedButton(
                          child: new Text("5")
                      ),
                      new RaisedButton(
                          child: new Text("6")
                      ),
                    ]
                ),
                new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      new RaisedButton(
                          child: new Text("7")
                      ),
                      new RaisedButton(
                          child: new Text("8")
                      ),
                      new RaisedButton(
                          child: new Text("9")
                      ),
                    ]
                )
              ]
          )
        ),
      );
  }

//  Widget getDialPadButtons() {
//
//    Row firstRow = new Row();
//    Row secondRow = new Row();
//    Row thirdRow = new Row();
//    Row fourthRow = new Row();
//
//    Row curRow = new Row();
//    for(var i = 1; i < 10; i++) {
//
//      if ( i < 4) {
//        curRow = firstRow;
//      } else if ( i < 7 ) {
//        curRow = secondRow;
//      } else {
//        curRow = thirdRow;
//      }
//
//      curRow.children.add(
//        new RaisedButton(
//            child: new Text("${i}")
//        )
//      );
//    }
//
//    return
//  }
}
