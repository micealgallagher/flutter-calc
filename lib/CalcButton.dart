import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

import 'package:flutter/material.dart';

class CalcButton extends RaisedButton {
  CalcButton({
    Key key,
    this.child,
    this.numberDisplay,
  }) : super(key: key, onPressed = () {});

  final Widget child;

  final Input numberDisplay;
}